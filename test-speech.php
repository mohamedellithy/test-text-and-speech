<?php 
/**
 * Plugin Name:  Question text and Speech
 * Plugin URI: http://QuestiontextandSpeech.com/
 * Description:Question text and Speech add any type of question
 * Author: mohamed ellithy
 * Author URI: https://mostaql.com/u/mohamed_ellithy
 * Version: 2.0.3
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Question-text-and-Speech
*/


if ( ! defined( 'ABSPATH' ) ) exit;

define( 'Quiz_VERSION', '2.0.3' );
define( 'Quiz_DB_VERSION', '2.0' );
define( 'Quiz_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'Quiz_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

register_activation_hook( __FILE__, 'do_action_and_create_page_when_activate_plugin' );
function do_action_and_create_page_when_activate_plugin(){
    if( empty(get_option('QST_Quiz_page_show_Quizs')) || !get_permalink(get_option('QST_Quiz_page_show_Quizs')) ):
		$post_data = array(
		    'post_title' => 'Show Quiz',
		    'post_content' => '[show-all-quizs]',
		    'post_status'  =>'publish',
		    'post_type' => 'page',
	    );
	    $post_id = wp_insert_post( $post_data );
	    update_option('QST_Quiz_page_show_Quizs',$post_id);
	endif;
}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}



require_once Quiz_PLUGIN_DIR.'/includes/functions.php';

