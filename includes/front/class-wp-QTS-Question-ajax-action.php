<?php 
class wp_QTS_Question_ajax_action{
	static $table_Qts_Quiz_results = 'Qts_Quiz_results';
	function __construct(){
        add_action('wp_ajax_QST_Questions_check_value_of_record',array($this,'QST_Questions_check_value_of_record'));
        add_action('wp_ajax_nopriv_QST_Questions_check_value_of_record',array($this,'QST_Questions_check_value_of_record'));

        add_action('wp_ajax_QST_Questions_check_choice_answer_is_true',array($this,'QST_Questions_check_choice_answer_is_true'));
        add_action('wp_ajax_nopriv_QST_Questions_check_choice_answer_is_true',array($this,'QST_Questions_check_choice_answer_is_true'));
	
        add_action('wp_ajax_QST_Questions_check_text_answer_is_true',array($this,'QST_Questions_check_text_answer_is_true'));
        add_action('wp_ajax_nopriv_QST_Questions_check_text_answer_is_true',array($this,'QST_Questions_check_text_answer_is_true'));
	    

        add_action('wp_ajax_QST_Quize_upgrade_degree_of_user',array($this,'QST_Quize_upgrade_degree_of_user'));
        add_action('wp_ajax_nopriv_QST_Quize_upgrade_degree_of_user',array($this,'QST_Quize_upgrade_degree_of_user'));
	
	   
	}

	function QST_Quize_upgrade_degree_of_user(){
		global $wpdb;
        $table_name = $wpdb->prefix . self::$table_Qts_Quiz_results;
        echo get_current_user_id();
        if(!empty(get_current_user_id())):
	            $wpdb->insert($table_name, array(
		               "User_id"  =>get_current_user_id(),
		               "Quiz_id"  =>$_POST['Quiz_ID'],
		               'degree'   =>$_POST['degree_of_test'],
		               'date_Quiz'=>date('d-m-Y'))
	                );
        	
        endif;
	}

	function QST_Questions_check_value_of_record(){
		$text_of_question = get_post_meta($_POST['Question_ID'],'correct_answer_of_audio',true); 
		if(!empty($text_of_question)){
			echo $text_of_question;
		}
		die();

	}

	function QST_Questions_check_choice_answer_is_true(){
		$answer = $_POST['answer_data'];
		$get_correct_answer_for_Question = get_post_meta($_POST['Question_ID'],'wp_QTS_Questions_correct_choices',true);
		foreach ($answer as $key => $value) {
		    if(in_array($value['value'],$get_correct_answer_for_Question)){
		    	$result = 'true';
		    }
			else
			{
				$result = 'false';
				break;
			}
		}

		echo $result;
		die();
	}


	function QST_Questions_check_text_answer_is_true(){
		$answer = $_POST['answer_data'];
		$get_correct_answer_for_Question = get_post_meta($_POST['Question_ID'],'wp_QTS_Questions_correct_text',true);
		foreach ($answer as $key => $value) {
		    if(strtolower($value['value'])==strtolower($get_correct_answer_for_Question)){
		    	$result = 'true';
		    }
			else
			{
				$result = 'false';
				break;
			}
		}

		echo $result;
		die();
	}


}