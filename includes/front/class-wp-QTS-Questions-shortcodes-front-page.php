<?php 

/**
**
**/
class wp_QTS_Questions_shortcodes_front_page extends wp_install_QTS_Questions_fields{
	public $all_questions;
	public $question_audio;
  public $number_of_attempts;
  public $quiz_arrange_Question;
  public $question_text;
  public $question_video;
	public $question_type;
	public $answers_type;
  public $notice_before_answer;
	public $questions_choices=array();
  public $questions_choices_correct;
	public $questions_text;
  public $questions_correct_text;
	function __construct(){
		add_shortcode('show_quiz_in_single_page',array($this,'show_quiz_in_single_page'));
		add_filter('the_content', array($this,'add_questions_for_quiz'));
       
	}

	function get_questions_for_quiz($post_id){
      $this->quiz_arrange_Question        = get_post_meta($post_id,'wp_QTS_Quiz_arrange_Question',true); 
      if(empty($this->quiz_arrange_Question)): 
           $this->all_questions = get_posts(array('posts_per_page'=>-1,'post_status'=>'publish','post_type'=>'qts_questions','meta_key'=>'wp_QTS_Questions_for_test','meta_value'=>$post_id));
	    else:
           $this->all_questions = get_posts(array('posts_per_page'=>-1,'post_status'=>'publish','post_type'=>'qts_questions','orderby' => 'post__in','post__in' => explode(',',$this->quiz_arrange_Question) ));
      endif;
      return $this->all_questions;
	}

	

	function show_quiz_in_single_page(){ 
      if(is_singular()): 
		    $this->before_single_question_slider();
        if( !empty($this->get_questions_for_quiz(get_the_ID()) ) ):
  		    foreach($this->get_questions_for_quiz(get_the_ID() ) as $Question): $this->get_all_questions_options_meta($Question->ID); ?>   
  			    <div>
  			        <?php $this->show_question_image($Question->ID); ?>
  			        <div class="content-question" >
                          <div>
      		               <div id="container_playlist<?php echo $Question->ID ?>" class="container_playlist" data-times="<?php echo (!empty($this->number_of_attempts)?$this->number_of_attempts:1); ?>">
                                  <?php $this->show_recodrd_audio_question(); ?>
                                  <?php $this->show_question_text(); ?>
                                  <?php $this->show_question_video(); ?>
                                  <div class="status_answer<?php echo $Question->ID ?>"></div>
                          </div>
      		                <?php  $this->show_notice_for_Question(); ?>
      			            <?php  $this->show_microphone_in_question($Question->ID); ?>
      			            <?php  $this->next_question_in_quiz($Question->ID); ?>
      			        </div>
                </div>
  			    </div>
    <?php   
          endforeach;
            $this->after_single_question_slider();
        endif;
      endif;
    }

    function get_all_questions_options_meta($question_id){
      $this->number_of_attempts  = get_post_meta($question_id,'number_of_attempts',true); 
    	$this->question_type       = get_post_meta($question_id,'wp_QTS_Questions_type',true); 
    	$this->question_audio      = get_post_meta($question_id,'wp_QTS_Questions_audio',true);
      $this->question_video      = get_post_meta($question_id,'wp_QTS_Questions_video',true);
      $this->question_text       = get_post_meta($question_id,'wp_QTS_Questions_text',true); 
    	$this->answers_type        = get_post_meta($question_id,'wp_QTS_answer_type',true); 
      $this->notice_before_answer= get_post_meta($question_id,'notice_before_answer',true);
    	$this->questions_notices   = get_post_meta($question_id,'wp_QTS_Question_notices',true);
    	$this->questions_choices   = get_post_meta($question_id,'wp_QTS_Questions_choices',true); 
    	$this->questions_choices_correct  = get_post_meta($question_id,'wp_QTS_Questions_correct_choices',true); 
      $this->questions_correct_text     = get_post_meta($question_id,'wp_QTS_Questions_correct_text',true);


    }

    function show_recodrd_audio_question(){
    	  if($this->question_type =='sound'){ ?>
          <div class="sect">
            	<audio class="QTS_Question_audio" controls>
        			  <source src="<?php echo $this->question_audio['audio_link']; ?>">
        			</audio>
		          <span class="QTS_Question_text_speeck"> <?php echo '__'.get_Question_Exerpt(10,$this->question_audio['text_of_audio']).'__'; ?> </span>
          </div>
    <?php 

        }
    }

    function show_question_image($question_id){
           if(empty($this->question_video)):  ?>
                <div class="text-center QTS-container-image-question"> 
                   	<?php echo get_the_post_thumbnail($question_id,'medium',array('class'=>'QTS-image-question')); ?> 
                </div> 
        <?php endif;
    }

    function show_question_text(){ 
        if($this->question_type =='text'): ?>
           <div class="QTS-question-text">
           	  <?php if(!empty($this->question_text)):  ?>
                        <?php echo  $this->question_text; ?>
              <?php endif; ?>
           </div>
    <?php 
        endif;
    }

    function show_notice_for_Question(){
      if(!empty($this->notice_before_answer)):?>

          <div class="notice_before_answer">
              <i class="fas fa-lightbulb"></i>
              <?php echo $this->notice_before_answer; ?>
          </div>

    <?php 
      endif;

    }

    function show_question_video(){ 
        if($this->question_type =='video'):
            if(!empty($this->question_video)): ?>
                <video width="320" height="240" controls>
                  <source src="<?php echo (!empty($this->question_video['Question_video_upload_link'])?$this->question_video['Question_video_upload_link']:(!empty($this->question_video['Question_video_link_only'])?$this->question_video['Question_video_link_only']:'')); ?>" type="video/mp4">
                  Your browser does not support the video tag.
                </video>
            <?php endif; ?>

           <div class="QTS-question-text">
              <?php if(!empty($this->question_video['text_of_video_uploaded'])):  ?>
                        <?php echo $this->question_video['text_of_video_uploaded']; ?>
              <?php endif; ?>
           </div>
    <?php 
        endif;
    }


    function before_single_question_slider(){ ?>
    	<div class="main">
        <div class="last-slide-result">here</div>
			<div class="slider slider-for">
    <?php 
    }

    function after_single_question_slider(){ ?>
                </div>
	        </div>
             <?php   
                  $this->add_fields(
                      [ 'name_field'   =>'',
                        'type_field'   =>'hidden',
                        'class_field'  =>'result_of_quiz',
                        'id_field'     =>'',
                        'value_field'  =>'',
                        'attributes'   =>'disabled',
                        
                      ]
                  );
              ?>
    <?php }

    function show_microphone_in_question($question_id){
        if($this->answers_type=='sound'): ?>
            <div  data-value="<?php echo $question_id; ?>" class="spech_record recordButton microphone<?php echo $question_id; ?> text-center"> 
	            <span class="icon_microphone"> 
	               <img class="recording-waves recording_instructions<?php echo $question_id; ?> " style="display:none" src="<?php echo Quiz_PLUGIN_URL.'assets/front/img/loading_record.gif'; ?>" /><br/>
	            	<i class="fas fa-microphone"></i>
	            </span>
                
	        </div>
             
    <?php  
        elseif($this->answers_type=='choices'):
             $this->show_choices_for_question($question_id);
        elseif($this->answers_type=='text'):
             $this->show_input_text($question_id);
        endif;
    }

    function show_choices_for_question($Question_id){
        if(!empty($this->questions_choices)): ?>
           
            <form method="POST" class="form_solution_question" data-value="<?php echo $Question_id; ?>">
                <table>
                <?php  foreach($this->questions_choices as $key=>$choices): ?>

                    <tr>
                        <td>
                            <?php  
                                $this->add_fields(
                                        [ 'name_field'   =>'answer',
                                          'type_field'   =>'checkbox',
                                          'class_field'  =>'choice_correct_choice',
                                          'id_field'     =>'',
                                          'value_field'  =>$key,
                                          'attributes'   =>'',
                                        ]
                                );
                            ?>
                        <span class="QTS_Question_text_speeck"> <?php echo $choices;  ?> </span> </td>
                    </tr>
                   
                <?php   endforeach; ?>

                </table>

    <?php 
            $this->show_button_check_question_answer($Question_id); 
        endif;
    }

    function show_button_check_question_answer($Question_ID){ ?>
           <button type="submit" id="submit<?php echo $Question_ID ?>" ><?php _e('Check Answer','QTS_Questions'); ?> </button>
        </form>
    <?php }

    function show_input_text($Question_id){
            if(!empty($this->questions_correct_text)): ?>
                <form method="POST" class="form_solution_correct_text" data-value="<?php echo $Question_id; ?>">
                    <div class="form-group QTS-container-input-text"> 
                        <label class=""><?php _e('Write Your Answer Here','QTS_Questions'); ?></label>
                        <?php  
                            $this->add_fields(
                                    [ 'name_field'   =>'correct_text',
                                      'type_field'   =>'text',
                                      'class_field'  =>'form-control correct_text',
                                      'id_field'     =>'',
                                      'value_field'  =>'',
                                      'attributes'   =>'',
                                      'required'     =>'required',
                                    ]
                            );
                        ?>
                    </div>
                           
    <?php
                $this->show_button_check_question_answer($Question_id); 
            endif;
    }

    function show_text_for_question(){
        if(!empty($this->questions_text)): ?>
         <h1> here choices </h1>
    <?php 
        endif;
    }

    

    function next_question_in_quiz($Question_id){ ?>
       <div class="container-next-slide"  id="next-question<?php echo $Question_id; ?>">
           <i class="fas fa-chevron-circle-right next-question" data-Quiz="<?php echo get_the_ID();  ?>" ></i>
       </div>
    <?php 
    
    }

    
			 
	function add_questions_for_quiz($content){
      if(get_post_type($post->ID)=='qts_quiz'):
          return $this->show_quiz_in_single_page(); 
      else:
         return $content;
      endif;
	} 


    


}