<?php 
class wp_QTS_Quiz_shortcodes_front_page{
     
     private $args=array();
     function __construct(){
         add_shortcode('show-all-quizs',array($this,'show_Quiz_card'));
         $this->get_all_QTS_Quizs();
     }

     function get_all_QTS_Quizs(){
            $this->args =   array( 'post_type'     =>'qts_quiz',
         	                    'posts_per_page'=>-1,
         	                    'post_status'   =>'publish',
         	                );    
     }

     function show_Quiz_card(){
     	    $get_all_QTS_Quizs=new WP_Query($this->args);
            if($get_all_QTS_Quizs->have_posts()):
            	while($get_all_QTS_Quizs->have_posts()):$get_all_QTS_Quizs->the_post();?>
			
			    <div class="card QTS_quiz_Card col-md-4">
			    	<div class="container-card">
						 <!--  <img src="..." class="card-img-top" alt="..."> -->
						 <?php the_post_thumbnail('',array('class'=>'card-img-top')); ?>
						  <div class="card-body">
						    <h5 class="card-title"><?php the_title(); ?></h5>
						    <p class="card-text"><?php echo get_custom_Exerpt(140); ?></p>
						    <a href="<?php echo the_permalink(); ?>" class="btn btn-primary"><?php _e('Take Quiz','QTS_Quiz'); ?></a>
						  </div>
			    	</div>
				</div>
			<?php
			    endwhile; 
			endif;


     }
}