<?php 
ob_start();


require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-QTS-Questions-fields.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-styles-scripts-files-theme.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-meta-QTS-Questions.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-meta-QTS-Quiz.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-post-types.php';

require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-QTS-Questions-save-posts.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-QTS-Quiz-save-posts.php';

require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-database-QTS-Quiz-results.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-create-show-result-pages.php';
require_once Quiz_PLUGIN_DIR.'includes/admin/class-wp-install-data-table-for-results-users.php';


require_once Quiz_PLUGIN_DIR.'includes/admin/helper_functions.php';
require_once Quiz_PLUGIN_DIR.'includes/front/class-wp-QTS-Questions-shortcodes-front-page.php';
require_once Quiz_PLUGIN_DIR.'includes/front/class-wp-QTS-Quiz-shortcodes-front-page.php';
require_once Quiz_PLUGIN_DIR.'includes/front/class-wp-QTS-Question-ajax-action.php';




/**
 * add theme options and settings
 * all options added by mohamed ellithy
 * @package 
 **/


show_admin_bar(false);




new wp_install_post_types();
new wp_install_styles_scripts_files_theme();

new wp_install_meta_QTS_Questions();
new wp_install_meta_QTS_Quiz();
//new wp_install_data_table_for_results_users();

new wp_install_QTS_Quiz_save_posts();
new wp_install_QTS_Questions_save_posts();

new wp_create_show_result_pages();
new wp_install_database_QTS_Quiz_results();


new wp_QTS_Questions_shortcodes_front_page();
new wp_QTS_Question_ajax_action();
new wp_QTS_Quiz_shortcodes_front_page();




add_theme_support( 'widgets' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'customize-selective-refresh-widgets' );
        









