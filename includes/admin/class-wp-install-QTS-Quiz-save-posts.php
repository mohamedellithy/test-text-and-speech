<?php 
/*
**save post for courses
*/
class wp_install_QTS_Quiz_save_posts {
    function __construct(){
         add_action( 'save_post', array($this,'save_post_QTS_Quiz'), 10, 2 );
	  }

	  function save_post_QTS_Quiz($post_id,$post){
        global $post,$wpdb;
	      if(!current_user_can('edit_post',$post_id)){
       	  return $post_id;
        }
        if(!empty($post->post_type)&&($post->post_type=='qts_quiz')){
        	  
            $meta_key['wp_QTS_Quiz_settings'] = array( 'quiz_number_of_attempts'=>
                                                    $_POST['quiz_number_of_attempts'],
                                                  'quiz_start_at'          =>$_POST['quiz_start_at'], 
                                                  'quiz_end_at'            =>$_POST['quiz_end_at'],
                                              );
           
            $meta_key['wp_QTS_Quiz_arrange_Question']  =implode(',',$_POST['Question_ID_Arrange']); 
           
            foreach ($meta_key as $key => $value){
                 if ( get_post_meta( $post_id, $key, false ) ) {
                  update_post_meta( $post_id, $key, $value );
                 } else {
                  add_post_meta( $post_id, $key, $value);
                }

            }
        }

    }
}