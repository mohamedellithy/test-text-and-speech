<?php 
/**
**  install fields 
**/
class wp_install_QTS_Questions_fields{
 
    public $QTS_Questions_fields=array();
 
    function add_fields($fields){
  
    	$this->QTS_Questions_fields = array($fields); 
   
    	foreach($this->QTS_Questions_fields as $key_field){
   
    		if(!in_array($key_field['type_field'],array('select','textarea')) ){
    			$fields_html .= "<input name='".$key_field['name_field']."' type='".$key_field['type_field']."'
    			 class='".$key_field['class_field']."' value='".$key_field['value_field']."' id='".$key_field['id_field']."' 
    			   ".$key_field['attributes']." ".(($_GET['action']!='edit')?$key_field['required']:'')." ".(!empty($key_field['checked'])?$key_field['checked']:'')." ".(!empty($key_field['readonly'])?$key_field['readonly']:'')." />";
    		}
            elseif($key_field['type_field']=='textarea'){
                $fields_html .= "<textarea name='".$key_field['name_field']."' 
                 class='".$key_field['class_field']."' cols='".$key_field['cols']."' rows='".$key_field['rows']."'  id='".$key_field['id_field']."' 
                   ".$key_field['attributes']." ".(!empty($key_field['checked'])?$key_field['checked']:'')." ".(!empty($key_field['readonly'])?$key_field['readonly']:'')." >".$key_field['value_field']."</textarea>";
            
            }
    		elseif($key_field['type_field']=='select'){
    			$fields_html .="<select name='".$key_field['name_field']."' class='".$key_field['class_field']."' id='".$key_field['id_field']."' 
    			        ".$key_field['attributes']." ".$key_field['required']." >";
                if(!empty($key_field['options'])){
        	        foreach($key_field['options'] as $key => $value) {
                        if(!empty($key_field['index_key']) && ($key_field['index_key']=='no_key')  ){
                            $fields_html .="<option value='".$value."' ".((!empty($key_field['value_active']) && ($key_field['value_active']==$value) )?'selected':'').">".$value."</option>";

                        }
                        else{
        	        	    $fields_html .="<option value='".$key."' ".((!empty($key_field['value_active']) && ($key_field['value_active']==$key) )?'selected':'').">".$value."</option>";
                        }
        	        }
                    
                }
    	        $fields_html .="</select>";
    		}
    
    	}

    	echo $fields_html;
    }

}