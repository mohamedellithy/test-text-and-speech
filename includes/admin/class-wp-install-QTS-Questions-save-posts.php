<?php 
/*
**save post for courses
*/
class wp_install_QTS_Questions_save_posts {
    function __construct(){
         add_action( 'save_post', array($this,'save_post_QTS_Questions'), 10, 2 );
	}

	function save_post_QTS_Questions($post_id,$post){
        global $post,$wpdb;
	      if(!current_user_can('edit_post',$post_id)){
       	  return $post_id;
        }
        if(!empty($post->post_type)&&($post->post_type=='qts_questions')){
        	
            $meta_key['wp_QTS_Questions_for_test']  =$_POST['Quiz_id']; 
            
            $get_last_arrange=get_post_meta($_POST['Quiz_id'],'wp_QTS_Quiz_arrange_Question',true);
            
            if( !in_array($post_id,explode(',', $get_last_arrange) ) ):
                update_post_meta($_POST['Quiz_id'],'wp_QTS_Quiz_arrange_Question',$post_id.','.$get_last_arrange);
            endif;

            $meta_key['number_of_attempts']         =$_POST['number_of_attempts'];
            $meta_key['wp_QTS_Questions_type']      =$_POST['field_type_question']; 
            if($_POST['field_type_question']=='sound'){
                $meta_key['wp_QTS_Questions_audio'] =['audio_link'=>$_POST['audio_link'],'audio_type'=>$_POST['audio_type'],'text_of_audio'=>$_POST['text_of_audio'] ]; 
            }
            elseif($_POST['field_type_question']=='text'){
                $meta_key['wp_QTS_Questions_text']      =$_POST['Question_Text']; 
            }
            elseif($_POST['field_type_question']=='video'){
                $meta_key['wp_QTS_Questions_video']     =['Question_video_upload_link'=>$_POST['Question_video_upload_link'],
                                                          'Question_video_link_only'  =>$_POST['Question_video_link_only'],
                                                          'text_of_video_uploaded'    =>$_POST['text_of_video_uploaded']
                                                         ]; 
            }

            $meta_key['wp_QTS_answer_type']             = $_POST['field_type_answer']; 
            $meta_key['notice_before_answer']           = $_POST['notice_before_answer']; 
           
            if($_POST['field_type_answer']=='sound'){
                $meta_key['correct_answer_of_audio']    = $_POST['correct_answer_of_audio'];
            }
            elseif($_POST['field_type_answer']=='choices'){
                $meta_key['wp_QTS_Questions_choices']   = array_filter($_POST['choices']);
                $meta_key['wp_QTS_Questions_correct_choices']=$_POST['correct']; 
            }
            elseif($_POST['field_type_answer']=='text'){
               $meta_key['wp_QTS_Questions_correct_text']=$_POST['correct_text']; 
            }
            

            
            foreach ($meta_key as $key => $value) {

                 if ( get_post_meta( $post_id, $key, false ) ) {
                  
                  update_post_meta( $post_id, $key, $value );
                 } else {
                  
                  add_post_meta( $post_id, $key, $value);
                }

            }

        	
        }
    }
}