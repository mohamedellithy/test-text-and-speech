<?php
/**
** 
**/ 
class wp_install_meta_QTS_Questions extends wp_install_QTS_Questions_fields{
	public $MetaBox;
	static $QTS_Questions='QTS_Questions';
	public $all_Quizs=array();
	public $quiz;
	public $question_audio;
    public $question_text;
    public $question_video;
	public $question_type;
	public $answers_type;
	public $notice_before_answer;
	public $number_of_attempts;
	public $questions_choices=array();
    public $questions_choices_correct;
    public $question_correct_answer_sound;
	public $questions_text;
    public $questions_correct_text;
	function __construct(){
 
		$this->MetaBox=[
            array(
                'metabox_id'               =>'_wp_QTS_Questions_vertical_tabs',
                'MyMetaBox'                => "<label>".__( 'Add Questions', 'QTS_Questions' )."</label>",
                'MetaBox_callback_function'=>array($this,'form_add_Questions'),
                'MetaBox_TypePostsToAdd'   =>self::$QTS_Questions,
                'MetaBox_Place'            =>'normal'
             
            )
        ];


        $this->register_MetaBoxes_For_QTS_Questions();
        $this->get_all_Quizs();
       
   

	}

	function form_add_Questions(){
		//echo "hi mohamed22";
		global $post;
		$this->get_all_questions_options_meta($post->ID);
		//$Course_Settings=get_post_meta($post->ID,'_WP_LMS_Course_Settings',true);
		require_once Quiz_PLUGIN_DIR.'/includes/templates/add-Questions/QTS_Questions_add_Question_form.php';    
	}

	 function get_all_questions_options_meta($question_id){
	 	
	 	$this->number_of_attempts            = get_post_meta($question_id,'number_of_attempts',true); 
	 	$this->quiz                          = get_post_meta($question_id,'wp_QTS_Questions_for_test',true); 
    	$this->question_type                 = get_post_meta($question_id,'wp_QTS_Questions_type',true); 
    	$this->question_audio                = get_post_meta($question_id,'wp_QTS_Questions_audio',true);
        $this->question_video                = get_post_meta($question_id,'wp_QTS_Questions_video',true);
        $this->question_text                 = get_post_meta($question_id,'wp_QTS_Questions_text',true); 
    	$this->answers_type                  = get_post_meta($question_id,'wp_QTS_answer_type',true); 
    	$this->notice_before_answer          = get_post_meta($question_id,'notice_before_answer',true);
    	$this->question_correct_answer_sound = get_post_meta($question_id,'correct_answer_of_audio',true);
    	$this->questions_notices             = get_post_meta($question_id,'wp_QTS_Question_notices',true);
    	$this->questions_choices             = get_post_meta($question_id,'wp_QTS_Questions_choices',true); 
    	$this->questions_choices_correct     = get_post_meta($question_id,'wp_QTS_Questions_correct_choices',true); 
        $this->questions_correct_text        = get_post_meta($question_id,'wp_QTS_Questions_correct_text',true);


    }

   

	function Install_MetaBoxes_For_QTS_Questions(){
		foreach ($this->MetaBox as $meta) {
		   add_meta_box($meta['metabox_id'],$meta['MyMetaBox'],$meta['MetaBox_callback_function'],$meta['MetaBox_TypePostsToAdd'],$meta['MetaBox_Place']);
		}
	}

	function register_MetaBoxes_For_QTS_Questions(){
		add_action('add_meta_boxes',array($this,'Install_MetaBoxes_For_QTS_Questions'));
	}

	function get_all_Quizs(){
         $all_quizs = get_posts(array('post_type'=>'qts_quiz','posts_per_page'=>-1));
         $this->all_Quizs[''] = '-select-';
         if(!empty($all_quizs)):
	         foreach ($all_quizs as $value):
	         	$this->all_Quizs[$value->ID] = get_the_title($value->ID);
	         endforeach;
	     endif;
		 return $this->all_Quizs;
	}

    

}