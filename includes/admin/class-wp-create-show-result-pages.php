<?php 
class wp_create_show_result_pages{

	public $create_sub_pages =array();

	function __construct(){
		$this->create_sub_pages [] =array(
                                 'under_page_slug'     =>'edit.php?post_type=qts_quiz',
                                 'page_title'          =>'QTS Quiz Results',
                                 'menu_title'          =>'QTS Quiz Results',
                                 'manage_options'      =>'activate_plugins',
                                 'slug'                =>'quiz_results',
                                 'action_run'          =>array($this,'Show_Quiz_Results'),
                                 'icon_page'           =>'',
                                 'position_of_page'    =>6,
			                    );
	    add_action('admin_menu', array($this,'student_add_menu_items') );
	}

	 
	 /** ************************ REGISTER THE TEST PAGE ****************************
	 *******************************************************************************
	 * Now we just need to define an admin page. For this example, we'll add a top-level
	 * menu item to the bottom of the admin menus.
	 */
	function student_add_menu_items(){
		if(!empty($this->create_sub_pages)):
			foreach($this->create_sub_pages as $page):
	            $hook =add_submenu_page( $page['under_page_slug'],$page['page_title'] , $page['menu_title'] , $page['manage_options'], $page['slug'], $page['action_run'] , $page['icon_page'] ,$page['position_of_page'] );
	            add_action( "load-$hook", array($this,'add_options') );
	        endforeach;
	    endif;

	} 

	function add_options() {
		  $option = 'per_page';
		  $args = array('label' => 'Books','default' => 10,'option' => 'books_per_page');
		  add_screen_option( $option, $args );
    }

	/** *************************** RENDER TEST PAGE ********************************
	 *******************************************************************************
	 * This function renders the admin page and the example list table. Although it's
	 * possible to call prepare_items() and display() from the constructor, there
	 * are often times where you may need to include logic here between those steps,
	 * so we've instead called those methods explicitly. It keeps things flexible, and
	 * it's the way the list tables are used in the WordPress core.
	 */
	function Show_Quiz_Results(){
	    
	    //Create an instance of our package class...
	    $testListTable = new wp_install_data_table_for_results_users();
	    //Fetch, prepare, sort, and filter our data...
	    $testListTable->prepare_items();
	    ?>
	    <div class="wrap">
	        
	        <div id="icon-users" class="icon32"><br/></div>
	        <h2> <?php _e('Show Results Quizs'); ?> </h2>
	        <?php if($_GET['action']=='edit'):?>
	        <?php else: ?>
	        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
	        <form id="movies-filter" method="get">
	            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
	            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
	            <?php //$testListTable->search_box( 'search', 'search_id' ); ?>
	            <!-- Now we can render the completed list table -->
	            <?php $testListTable->display() ?>
	        </form>
	        <?php endif; ?>

	        
	        
	    </div>
	    <?php
	}


}