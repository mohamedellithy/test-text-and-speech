<?php 
class wp_install_meta_QTS_Quiz extends wp_install_QTS_Questions_fields{

    static $QTS_Quiz ='QTS_Quiz';
    public $questions_arrange;
    public $quiz_settings; 
    public $all_questions_for_quiz;
	function __construct(){

		$this->MetaBox=[
            array(
                'metabox_id'               =>'_wp_QTS_Quiz_vertical_tabs',
                'MyMetaBox'                => "<label>".__( 'Setting Quiz', 'QTS_Questions' )."</label>",
                'MetaBox_callback_function'=>array($this,'setting_quiz_options'),
                'MetaBox_TypePostsToAdd'   =>self::$QTS_Quiz,
                'MetaBox_Place'            =>'normal'
             
            ),
            array(
                'metabox_id'               =>'_wp_QTS_Quiz_show_question_vertical_tabs',
                'MyMetaBox'                => "<label>".__( 'show Questions and arrange', 'QTS_Questions' )."</label>",
                'MetaBox_callback_function'=>array($this,'show_all_questions_in_quiz'),
                'MetaBox_TypePostsToAdd'   =>self::$QTS_Quiz,
                'MetaBox_Place'            =>'normal'
             
            )
        ];


        $this->register_MetaBoxes_For_QTS_Quiz();
       
   

	}

	function setting_quiz_options(){
		global $post;
		//$this->get_all_questions_options_meta($post->ID);
		$this->get_quiz_settings();
		require_once Quiz_PLUGIN_DIR.'/includes/templates/add-Questions/QTS_Questions_setting_options_for_quiz.php';    	
	}

	function show_all_questions_in_quiz(){
		global $post;
		$this->get_questions_orders();
		$this->get_questions_for_quiz($post->ID);

		require_once Quiz_PLUGIN_DIR.'/includes/templates/add-Questions/QTS_Questions_show_questions_and_arrange_quiz.php';    	
	}

	function get_quiz_settings(){
		global $post;
		$this->quiz_settings = get_post_meta($post->ID,'wp_QTS_Quiz_settings',true);
	}

	function get_questions_orders(){
		global $post;
		$this->questions_arrange = get_post_meta($post->ID,'wp_QTS_Quiz_arrange_Question',true);
	}

	function get_questions_for_quiz($question_id){
		if(empty($this->questions_arrange)):
            $args_get_questions = array(
		                           'post_type'=>'qts_questions',
		                           'meta_key'=>'wp_QTS_Questions_for_test',
		                           'meta_value'=>$question_id,
		                           'posts_per_page'=>-1,
		                        );    
                                      
        else:

        	$args_get_questions = array(
		                           'post_type'  =>'qts_questions',
		                           'orderby' => 'post__in', 
                                   'post__in' => explode(',',$this->questions_arrange),
                                   'posts_per_page'=>-1,
		                        ); 

        endif;

        $this->all_questions_for_quiz =get_posts($args_get_questions);

	}

	function Install_MetaBoxes_For_QTS_Quiz(){
		foreach ($this->MetaBox as $meta) {
		   add_meta_box($meta['metabox_id'],$meta['MyMetaBox'],$meta['MetaBox_callback_function'],$meta['MetaBox_TypePostsToAdd'],$meta['MetaBox_Place']);
		}
	}

	function register_MetaBoxes_For_QTS_Quiz(){
		add_action('add_meta_boxes',array($this,'Install_MetaBoxes_For_QTS_Quiz'));
	}

}