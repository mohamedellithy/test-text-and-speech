<?php 
/**
* register post type to add in theme travelo
* add post type [ Questions ]
**/
class wp_install_post_types{
	public $post_type=array();
	static $Questions_Post_Type = 'QTS_Questions';
	static $Quiz_Post_Type = 'QTS_Quiz';
	function __construct(){
	    $this->post_type=[
            array(
                    'name'               => _x( self::$Questions_Post_Type, 'post type general name', 'your-plugin-textdomain' ),
					'singular_name'      => _x( self::$Questions_Post_Type, 'post type singular name', 'your-plugin-textdomain' ),
					'menu_name'          => _x( self::$Questions_Post_Type, 'admin menu', 'your-plugin-textdomain' ),
					'name_admin_bar'     => _x( self::$Questions_Post_Type, 'add new on admin bar', 'your-plugin-textdomain' ),
					'add_new'            => _x( 'Add New '.self::$Questions_Post_Type, 'book', 'your-plugin-textdomain' ),
					'add_new_item'       => __( 'Add New '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'new_item'           => __( 'New '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'edit_item'          => __( 'Edit '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'view_item'          => __( 'View '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'all_items'          => __( 'All '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'search_items'       => __( 'Search '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'parent_item_colon'  => __( 'Parent '.self::$Questions_Post_Type.' : ', 'your-plugin-textdomain' ),
					'not_found'          => __( 'No '.self::$Questions_Post_Type, 'your-plugin-textdomain' ),
					'not_found_in_trash' => __( 'No '.self::$Questions_Post_Type.' found in Trash.', 'your-plugin-textdomain' ),
	                /********************************************************************/
	                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
					'public'             => true,
					'publicly_queryable' => true,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'query_var'          => true,
					'rewrite'            => array( 'slug' => self::$Questions_Post_Type ),
					'capability_type'    => 'post',
					'taxonomies'          => array(),
					'has_archive'        => true,
					'hierarchical'       => true,
					'exclude_from_search' => false,
					'publicly_queryable'  => true,
					'menu_position'      => null,
					'menu_icon'           => 'dashicons-list-view',
					'supports'           => array( 'thumbnail','title')
	            ),
                array(
                    'name'               => _x( self::$Quiz_Post_Type, 'post type general name', 'your-plugin-textdomain' ),
					'singular_name'      => _x( self::$Quiz_Post_Type, 'post type singular name', 'your-plugin-textdomain' ),
					'menu_name'          => _x( self::$Quiz_Post_Type, 'admin menu', 'your-plugin-textdomain' ),
					'name_admin_bar'     => _x( self::$Quiz_Post_Type, 'add new on admin bar', 'your-plugin-textdomain' ),
					'add_new'            => _x( 'Add New '.self::$Quiz_Post_Type, 'book', 'your-plugin-textdomain' ),
					'add_new_item'       => __( 'Add New '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'new_item'           => __( 'New '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'edit_item'          => __( 'Edit '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'view_item'          => __( 'View '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'all_items'          => __( 'All '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'search_items'       => __( 'Search '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'parent_item_colon'  => __( 'Parent '.self::$Quiz_Post_Type.' : ', 'your-plugin-textdomain' ),
					'not_found'          => __( 'No '.self::$Quiz_Post_Type, 'your-plugin-textdomain' ),
					'not_found_in_trash' => __( 'No '.self::$Quiz_Post_Type.' found in Trash.', 'your-plugin-textdomain' ),
	                /********************************************************************/
	                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
					'public'             => true,
					'publicly_queryable' => true,
					'show_ui'            => true,
					'show_in_menu'       => true,
					'query_var'          => true,
					'rewrite'            => array( 'slug' => self::$Quiz_Post_Type ),
					'capability_type'    => 'post',
					'taxonomies'          => array(),
					'has_archive'        => true,
					'hierarchical'       => true,
					'exclude_from_search' => false,
					'publicly_queryable'  => true,
					'menu_position'      => null,
					'menu_icon'           => 'dashicons-list-view',
					 'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	            )
        ];

       
       /**
       *  run code install post type here 
       **/
       $this->add_custom_post_type();

	}



	function install_course_post_type(){
		foreach ($this->post_type as $post) {   
            register_post_type($post['name'],
	                array(       
               	        'labels'   =>array(
	                   	'name'               =>$post['name'],
						'singular_name'      =>$post['singular_name'],
						'menu_name'          =>$post['menu_name'],
						'name_admin_bar'     =>$post['name_admin_bar'],
						'add_new'            =>$post['add_new'],
						'add_new_item'       =>$post['add_new_item'],
						'new_item'           =>$post['new_item'],									
						'edit_item'          =>$post['edit_item'],									
						'view_item'          =>$post['view_item'],										
						'all_items'          =>$post['all_items'],										
						'search_items'       =>$post['search_items'],										
						'parent_item_colon'  =>$post['parent_item_colon'],										
						'not_found'          =>$post['not_found'],
						'not_found_in_trash' =>$post['not_found_in_trash']
   	                ),
   	                'description'        =>$post['description'], 
				    'public'             =>$post['public'], 
				    'publicly_queryable' =>$post['publicly_queryable'], 
				    'show_ui'            =>$post['show_ui'], 
				    'show_in_menu'       =>$post['show_in_menu'], 
				    'query_var'          =>$post['query_var'], 
				    'rewrite'            =>$post['rewrite'],  
				    'capability_type'    =>$post['capability_type'], 
				    'taxonomies'         =>$post['taxonomies'],
				    'has_archive'        =>$post['has_archive'], 
				    'hierarchical'       =>$post['hierarchical'], 
				    'exclude_from_search'=>$post['exclude_from_search'],
				     'publicly_queryable'=>$post['publicly_queryable'],
				    'menu_position'      =>$post['menu_position'], 
				    'menu_icon'          =>$post['menu_icon'],
				    'supports'           =>$post['supports'], 
			));

		}
			
	}

	function add_custom_post_type(){
		add_action( 'init', array($this,'install_course_post_type'), 0 );
	}

}