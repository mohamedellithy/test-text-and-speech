<?php
/**
 * elzehore theme options install styles and scripts in theme class class_install_styles_scripts_file_theme()
 * @package WordPress
 * @subpackage QST_Questions
 * @since 1.0
 *
 **/ 
class wp_install_styles_scripts_files_theme{
     
     function __construct(){

     	 add_action('wp_enqueue_scripts',array($this,'install_styles_css_theme'));

     	 add_action('wp_enqueue_scripts',array($this,'install_scripts_js_theme'));


         add_action('admin_enqueue_scripts',array($this,'install_admin_styles_css_theme'));

         add_action('admin_enqueue_scripts',array($this,'install_admin_scripts_js_theme'));
         
     }

     function install_styles_css_theme(){
     	 global $wp_styles;

         wp_enqueue_style('QST_Questions_google_apis_font','https://fonts.googleapis.com/css?family=Montserrat:400,700',array(),'');
         wp_enqueue_style('QST_Questions_google_apis_font_cairo','https://fonts.googleapis.com/css?family=Cairo&display=swap',array(),'');
         wp_enqueue_style('QST_Questions_bootstrap_Css',Quiz_PLUGIN_URL.'assets/front/css/bootstrap.min.css',array(),'');
         wp_enqueue_style('QST_Questions_fontawesome',Quiz_PLUGIN_URL.'assets/front/css/font-awesome.css',array(),'');
         wp_enqueue_style('QST_Questions_slick_theme',Quiz_PLUGIN_URL.'assets/front/css/slick-theme.css',array(),''); 
         wp_enqueue_style('QST_Questions_slick',Quiz_PLUGIN_URL.'assets/front/css/slick.css',array(),'');
         wp_enqueue_style('QST_Questions_style',Quiz_PLUGIN_URL.'assets/front/css/style.css',array(),'');
         

     }

     function install_scripts_js_theme(){
        global $wp_scripts;
        wp_enqueue_script('jquery');
        wp_enqueue_script('QST_Questions_font_awesom_icon','https://kit.fontawesome.com/f6267473b9.js',array(),'',false);
        wp_enqueue_script('QST_Questions_record_js','https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js',array(),'',true);
        wp_enqueue_script('QST_Questions_bootstrap',Quiz_PLUGIN_URL.'assets/front/js/bootstrap.min.js',array(),'',true);
        wp_enqueue_script('QST_Questions_slick',Quiz_PLUGIN_URL.'assets/front/js/slick.min.js',array(),'',true);
        wp_enqueue_script('QST_Questions_script',Quiz_PLUGIN_URL.'assets/front/js/script.js',array(),'',true);
        wp_enqueue_script('QST_Questions_run_code_record',Quiz_PLUGIN_URL.'assets/front/js/run_code_record.js',array(),'',true);
	 	
        /**
          * add script ajax here 
          **/
        wp_enqueue_script('QST_Questions_script_ajax',Quiz_PLUGIN_URL.'assets/front/js/ajax_script.js',array(),'',true);
        wp_localize_script('QST_Questions_script_ajax','QST_Questions_action_ajax',array('ajaxurl' => admin_url('admin-ajax.php') ) );
        
     }




      function install_admin_styles_css_theme(){
         global $wp_styles;
         wp_enqueue_style('QST_Questions_google_apis_font','https://fonts.googleapis.com/css?family=Montserrat:400,700',array(),'');
         wp_enqueue_style('QST_Questions_google_apis_font_cairo','https://fonts.googleapis.com/css?family=Cairo&display=swap',array(),'');
         wp_enqueue_style('QST_Questions_admin_bootstrap_Css',Quiz_PLUGIN_URL.'assets/admin/css/bootstrap.min.css',array(),'');
         wp_enqueue_style('QST_Questions_admin_fontawesome',Quiz_PLUGIN_URL.'assets/admin/css/font-awesome.css',array(),'');
         wp_enqueue_style('QST_Questions_admin_selectize_css',Quiz_PLUGIN_URL.'assets/admin/css/selectize.min.css',array(),'');
         wp_enqueue_style('QST_Questions_admin_style',Quiz_PLUGIN_URL.'assets/admin/css/admin_style.css',array(),'');
         

     }

     function install_admin_scripts_js_theme(){
        global $wp_scripts;
        wp_enqueue_script('jquery');
        //wp_enqueue_script('QST_Questions_admin_jQuery',Quiz_PLUGIN_URL.'assets/admin/js/jquery.js',array(),'',false);
        wp_enqueue_script('QST_Questions_admin_bootstrap',Quiz_PLUGIN_URL.'assets/admin/js/bootstrap.min.js',array(),'',true);
        wp_enqueue_script('QST_Questions_admin_selectize',Quiz_PLUGIN_URL.'assets/admin/js/selectize.min.js',array(),'',true);
        wp_enqueue_script('QST_Questions_admin_script',Quiz_PLUGIN_URL.'assets/admin/js/admin_script.js',array(),'',true);
        
        /**
          * add script ajax here 
          **/
        wp_enqueue_script('QST_Questions_ajax_upload_images_Question',Quiz_PLUGIN_URL.'assets/admin/js/upload_images_Question.js',array(),'',true);
        wp_localize_script('QST_Questions_ajax_upload_images_Question','upload_images_action_ajax',array('ajaxurl' => admin_url('admin-ajax.php') ) );
      
       /**
          * add script ajax here 
          **/
      /*  wp_enqueue_script('QST_Questions_ajax_add_new_appointment_flights',Quiz_PLUGIN_URL.'assets/admin/js/add_new_appointment_flights.js',array(),'',true);
        wp_localize_script('QST_Questions_ajax_add_new_appointment_flights','add_new_appointment_flights',array('ajaxurl' => admin_url('admin-ajax.php') ) );
     */
    
     }

}