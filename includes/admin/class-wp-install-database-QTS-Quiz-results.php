<?php 

class wp_install_database_QTS_Quiz_results{
    static $table_Qts_Quiz_results = 'Qts_Quiz_results';
    function __construct(){
       $this->create_database_for_quiz_results();
       //$this->insert_data_for_test();
    }

    function create_database_for_quiz_results(){
            
            global $wpdb;
            $table_name = $wpdb->prefix . self::$table_Qts_Quiz_results;
    
		    if($wpdb->get_var(" SHOW TABLES LIKE '$table_name'") != $table_name ){
		            $charset_collate = $wpdb->get_charset_collate();
		        
		            $sql = "CREATE TABLE $table_name (
		              ID mediumint(9) NOT NULL AUTO_INCREMENT,
		              User_id int(255) NOT NULL ,
		              Quiz_id int(255) NOT NULL ,
		              degree varchar(255) NOT NULL ,
		              date_Quiz varchar(255) NOT NULL ,
		              PRIMARY KEY  (id)
		            ) $charset_collate;";
		            
		            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		            
		            dbDelta( $sql );
		    }
    }

    /*function insert_data_for_test(){

    	global $wpdb;
        $table_name = $wpdb->prefix . self::$table_Qts_Quiz_results;
        $wpdb->insert($table_name, array(
	               "User_id"  =>3,
	               "Quiz_id"  =>7651,
	               'degree'   =>12,
	               'date_Quiz'=>date('d-m-Y'))
                );

    }*/

    
}