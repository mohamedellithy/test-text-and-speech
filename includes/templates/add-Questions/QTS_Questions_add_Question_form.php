<?php
   //var_dump($this->question_type); 
?>
<div class="conatiner">
   
        <div class="section-choose-type-Question">
            
            <div class="form-group">
              <label class="label_choose_type">
                  <span class="dashicons dashicons-admin-generic"></span>
                  <?php _e('choice Quiz','QTS_Questions'); ?>
              </label>

              <div class="single-choices ">
                  <?php  $this->add_fields(
                              [ 'name_field'   =>'Quiz_id',
                                'type_field'   =>'select',
                                'class_field'  =>'form-control',
                                'id_field'     =>'',
                                'options'      =>(!empty($this->all_Quizs)?$this->all_Quizs:array()),
                                'value_active' =>(!empty($this->quiz)?$this->quiz:''),
                              ]
                          );
                  ?>
              </div>
            </div><div class="clearfix"></div>


            <div class="form-group">
              <label class="label_choose_type">
                  <span class="dashicons dashicons-admin-generic"></span>
                  <?php _e('Number of attempts','QTS_Questions'); ?>
              </label>

              <div class="single-choices ">
                  <?php  $this->add_fields(
                              [ 'name_field'   =>'number_of_attempts',
                                'type_field'   =>'number',
                                'class_field'  =>'',
                                'id_field'     =>'',
                                'value_field'  =>(!empty($this->number_of_attempts)?$this->number_of_attempts:1),
                                'attributes'   =>'',
                              ]
                          );
                  ?>
              </div>

            </div><div class="clearfix"></div>

            <div class="form-group">
                <label class="label_choose_type">
                 	<span class="dashicons dashicons-admin-generic"></span>
                	<?php _e('Type Question','QTS_Questions'); ?>
                </label>
	            
      			    <div class="container-choices">
                    <div class="single-choices">
                         <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_question',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'type_of_Question',
                                      'id_field'     =>'type_question_sound',
                                      'value_field'  =>'sound',
                                      'attributes'   =>'',
                                      'checked'     =>((empty($this->question_type) || ($this->question_type=='sound'))?'checked':''),
                                    ]
                                );
                        ?>
                    	<label for="type_question_sound"> <span class="dashicons dashicons-microphone"></span> sound</label>
                    </div>
                    <div class="single-choices">
                        <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_question',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'type_of_Question',
                                      'id_field'     =>'type_question_text',
                                      'value_field'  =>'text',
                                      'attributes'   =>'',
                                      'checked'     =>((!empty($this->question_type) && ($this->question_type=='text'))?'checked':''),
                                    ]
                                );
                        ?>
                    	<label for="type_question_text"> <span class="dashicons dashicons-editor-paste-text"></span> text</label>
                    </div>
                    <div class="single-choices">
                        <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_question',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'type_of_Question',
                                      'id_field'     =>'type_question_video',
                                      'value_field'  =>'video',
                                      'attributes'   =>'',
                                      'checked'     =>((!empty($this->question_type) && ($this->question_type=='video'))?'checked':''),
                                    ]
                                );
                        ?>
                    	<label for="type_question_video"><span class="dashicons dashicons-video-alt3"></span> video</label>
                    </div>
      			    </div>

               
                <div class="form-add-Question form-speech <?php echo ((empty($this->question_type)|| ($this->question_type=='sound'))?'active':'no_active'); ?> ">
                   
                    <table class="options">
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Upload speech / Audio','QTS_Questions'); ?></th>
                            <td> 
                                <button class="button upload_audio_question" type="button" > <span class="dashicons dashicons-upload"></span> upload  </button> 
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'audio_link',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control readonly_class audio_link',
                                              'id_field'     =>'',
                                              'value_field'  =>(!empty($this->question_audio['audio_link'])?$this->question_audio['audio_link']:''),
                                              'attributes'   =>'readonly',
                                              
                                            ]
                                        );
                                ?>
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'audio_type',
                                              'type_field'   =>'hidden',
                                              'class_field'  =>'form-control audio_type',
                                              'id_field'     =>'',
                                              'value_field'  =>'',
                                              'attributes'   =>'',

                                            ]
                                        );
                                ?>
                            </td>
                            
                        </tr>
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Text Of Audio','QTS_Questions'); ?></th>
                            <td> 
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'text_of_audio',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'placeholder'  =>'Text Of Audio',
                                              'value_field'  =>(!empty($this->question_audio['text_of_audio'])?$this->question_audio['text_of_audio']:''),
                                              'attributes'   =>'',
                                            ]
                                        );
                                ?>
                            </td>
                        </tr>
                       
                    </table>
                </div>

                <div class="form-add-Question form-text <?php echo ((!empty($this->question_type) && ($this->question_type=='text'))?'active':'no_active'); ?> ">
                    <table class="options">
                        
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Text Of Question','QTS_Questions'); ?></th>
                            <td> 
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'Question_Text',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'placeholder'  =>'Question Text',
                                              'value_field'  =>(!empty($this->question_text)?$this->question_text:''),
                                              'attributes'   =>'',
                                            ]
                                        );
                                ?>
                            </td>
                        </tr>
                        
                    </table>
                </div>

                <div class="form-add-Question form-video <?php echo ((!empty($this->question_type) && ($this->question_type=='video'))?'active':'no_active'); ?> " >
                    <table class="options">
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Upload Video','QTS_Questions'); ?></th>
                            <td> 
                                <button class="button upload_video_question" type="button" ><span class="dashicons dashicons-upload"></span> upload  </button> 
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'Question_video_upload_link',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control readonly_class',
                                              'id_field'     =>'Question_video_upload_link',
                                              'placeholder'  =>'Question video upload',
                                              'value_field'  =>(!empty($this->question_video['Question_video_upload_link'])?$this->question_video['Question_video_upload_link']:''),
                                              'attributes'   =>'readonly',
                                            ]
                                        );
                                ?>
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'Question_video_upload_type',
                                              'type_field'   =>'hidden',
                                              'class_field'  =>'form-control ',
                                              'id_field'     =>'Question_video_upload_type',
                                              'placeholder'  =>'Question video upload',
                                              'value_field'  =>'',
                                              'attributes'   =>'',
                                            ]
                                        );
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Link of Video','QTS_Questions'); ?></th>
                            <td> 
                                <?php   $this->add_fields(
                                            [ 'name_field'   =>'Question_video_link_only',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'placeholder'  =>'Question video link only',
                                              'value_field'  =>(!empty($this->question_video['Question_video_link_only'])?$this->question_video['Question_video_link_only']:''),
                                              'attributes'   =>'',
                                            ]
                                        );
                                ?>
                             </td>
                        </tr>
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Text of Video','QTS_Questions'); ?></th>
                            <td> 
                                 <?php   $this->add_fields(
                                            [ 'name_field'   =>'text_of_video_uploaded',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'placeholder'  =>'text of video uploaded',
                                              'value_field'  =>(!empty($this->question_video['text_of_video_uploaded'])?$this->question_video['text_of_video_uploaded']:''),
                                              'attributes'   =>'',
                                            ]
                                        );
                                ?>
                            </td>
                        </tr>
                        
                    </table>
                </div>

            </div>
            <div class="form-group">
                <label class="label_choose_type">
                    <span class="dashicons dashicons-admin-generic"></span>
                    <?php _e('Notice Before answer','QTS_Questions'); ?>
                </label>

                <div class="single-choices QTS_container_notice_area">
                    <?php  $this->add_fields(
                                [ 'name_field'   =>'notice_before_answer',
                                  'type_field'   =>'textarea',
                                  'class_field'  =>'',
                                  'id_field'     =>'',
                                  'value_field'  =>(!empty($this->notice_before_answer)?$this->notice_before_answer:''),
                                  'attributes'   =>'',
                                  'cols'         =>'60',
                                  'rows'         =>'2',
                                ]
                            );
                    ?>
                </div>

            </div><div class="clearfix"></div>
            <div class="form-group">
                <label class="label_choose_type">
                    <span class="dashicons dashicons-admin-generic"></span>
                    <?php _e('Type Answers','QTS_Questions'); ?>
                </label>
                
                <div class="container-choices">
                    <div class="single-choices">
                        <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_answer',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'field_type_answer',
                                      'id_field'     =>'type_answer_sound',
                                      'value_field'  =>'sound',
                                      'attributes'   =>'',
                                      'checked'     =>((empty($this->answers_type) || ($this->answers_type=='sound'))?'checked':''),
                                    ]
                                );
                        ?>
                        <label for="type_answer_sound"> <span class="dashicons dashicons-microphone"></span> sound</label>
                    </div>
                    <div class="single-choices">
                        <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_answer',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'field_type_answer',
                                      'id_field'     =>'type_answer_choices',
                                      'value_field'  =>'choices',
                                      'attributes'   =>'',
                                      'checked'     =>((!empty($this->answers_type) && ($this->answers_type=='choices'))?'checked':''),
                                    ]
                                );
                        ?>
                        <label for="type_answer_choices"><span class="dashicons dashicons-editor-justify"></span> choices</label>
                    </div>
                    <div class="single-choices">
                        <?php  $this->add_fields(
                                    [ 'name_field'   =>'field_type_answer',
                                      'type_field'   =>'radio',
                                      'class_field'  =>'field_type_answer',
                                      'id_field'     =>'type_answer_text',
                                      'value_field'  =>'text',
                                      'attributes'   =>'',
                                      'checked'     =>((!empty($this->answers_type) && ($this->answers_type=='text'))?'checked':''),
                                    ]
                                );
                        ?>
                        <label for="type_answer_text"> <span class="dashicons dashicons-editor-paste-text"></span> text</label>
                    </div>
                </div>

                <div class="form-add-answer answer-speech <?php echo ((empty($this->answers_type)|| ($this->answers_type=='sound'))?'active':'no_active'); ?> ">
                    <table class="options">
                        
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Correct Answer of Audio','QTS_Questions'); ?></th>
                            <td> 
                                <?php  $this->add_fields(
                                        [ 'name_field'   =>'correct_answer_of_audio',
                                          'type_field'   =>'text',
                                          'class_field'  =>'correct_answer_of_audio',
                                          'id_field'     =>'correct_answer_of_audio',
                                          'value_field'  =>(!empty($this->question_correct_answer_sound)?$this->question_correct_answer_sound:''),
                                          'attributes'   =>'',
                                        ]
                                    );
                                ?>
                             </td>
                        </tr>
                        
                       
                    </table>
                </div>

                <div class="form-add-answer answer-choices <?php echo ((!empty($this->answers_type)&& ($this->answers_type=='choices'))?'active':'no_active'); ?> ">
                    <table class="options">
                        
                        <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Add Choices ','QTS_Questions'); ?></th>
                        </tr>
                        <?php if(!empty($this->questions_choices)):$number_of_choice = 0; $index_of_choice = 1; ?>
                        <?php foreach($this->questions_choices as $key=>$choice): ?>
                            <tr class="choice<?php echo intval($number_of_choice)+1; ?> tr_table_choices">
                                <td> 
                                    <label style="padding: 7px;">#choice<?php echo intval($number_of_choice)+1; ?></label> 
                                    <?php  $this->add_fields(
                                            [ 'name_field'   =>'correct[]',
                                              'type_field'   =>'checkbox',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'value_field'  =>$number_of_choice,
                                              'attributes'   =>'',
                                              'checked'     =>((empty($this->questions_choices_correct) || (in_array($key,$this->questions_choices_correct)))?'checked':''),

                                            ]
                                        );
                                    ?>
                                   
                                </td>
                                <td>
                                    <?php  $this->add_fields(
                                            [ 'name_field'   =>'choices[]',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'value_field'  =>$choice,
                                              'attributes'   =>'',
                                            ]
                                        );
                                    ?>
                                </td>
                              <?php if($number_of_choice==0): ?>
                                  <td> <span class="dashicons dashicons-plus add-new-choise"></span> </td>
                              <?php endif; ?>
                            </tr>
                        <?php $number_of_choice+=1; ?>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <tr class="choice1 tr_table_choices">
                                <td> 
                                    <label style="padding: 7px;">#choice1</label> 
                                    <?php  $this->add_fields(
                                            [ 'name_field'   =>'correct[]',
                                              'type_field'   =>'checkbox',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'value_field'  =>0,
                                              'attributes'   =>'',
                                            ]
                                        );
                                    ?>
                                   
                                </td>
                                <td>
                                    <?php  $this->add_fields(
                                            [ 'name_field'   =>'choices[]',
                                              'type_field'   =>'text',
                                              'class_field'  =>'form-control',
                                              'id_field'     =>'',
                                              'value_field'  =>'',
                                              'attributes'   =>'',
                                            ]
                                        );
                                    ?>
                                </td>
                              
                                <td> <span class="dashicons dashicons-plus add-new-choise"></span> </td>
                             
                            </tr>
                      
                        <?php endif; ?>
                         
                        
                    </table>
                </div>

                <div class="form-add-answer answer-text <?php echo ((!empty($this->answers_type)&& ($this->answers_type=='text'))?'active':'no_active'); ?>">
                    <table class="options">
                         <tr>
                            <th> <span class="dashicons dashicons-arrow-right"></span> <?php _e('Correct Answer ','QTS_Questions'); ?></th>
                            <td> 
                               <?php  $this->add_fields(
                                        [ 'name_field'   =>'correct_text',
                                          'type_field'   =>'text',
                                          'class_field'  =>'form-control',
                                          'id_field'     =>'',
                                          'value_field'  =>(!empty($this->questions_correct_text)?$this->questions_correct_text:''),
                                          'attributes'   =>'',
                                        ]
                                    );
                                ?> 
                            </td>
                        </tr>
                        
                        
                    </table>
                </div>

            </div>

        </div>

</div>