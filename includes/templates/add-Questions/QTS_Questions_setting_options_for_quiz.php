<div class="conatiner">
    <div class="section-choose-type-Question">
        <div class="form-group">
          <label class="label_choose_type">
              <span class="dashicons dashicons-admin-generic"></span>
              <?php _e('Number of attempts','QTS_Questions'); ?>
          </label>

          <div class="single-choices ">
              <?php  $this->add_fields(
                          [ 'name_field'   =>'quiz_number_of_attempts',
                            'type_field'   =>'number',
                            'class_field'  =>'form-control',
                            'id_field'     =>'',
                            'value_field'  =>(!empty($this->quiz_settings['quiz_number_of_attempts'])?$this->quiz_settings['quiz_number_of_attempts']:1),
                            'attributes'   =>'',
                          ]
                      );
              ?>
          </div>

        </div>
        <!-- <div class="clearfix"></div>
        <div class="form-group">
          <label class="label_choose_type">
              <span class="dashicons dashicons-admin-generic"></span>
              <?php //_e('Time of Quiz','QTS_Questions'); ?>
          </label>

          <div class="single-choices ">
              <?php  /*$this->add_fields(
                          [ 'name_field'   =>'number_of_attempts',
                            'type_field'   =>'number',
                            'class_field'  =>'form-control',
                            'id_field'     =>'',
                            'value_field'  =>(!empty($this->number_of_attempts)?$this->number_of_attempts:1),
                            'attributes'   =>'',
                          ]
                      );*/
              ?>
          </div>

        </div> -->
        <div class="clearfix"></div>
        <div class="form-group">
          <label class="label_choose_type">
              <span class="dashicons dashicons-admin-generic"></span>
              <?php _e('Quiz start at','QTS_Questions'); ?>
          </label>

          <div class="single-choices ">
              <?php  $this->add_fields(
                          [ 'name_field'   =>'quiz_start_at',
                            'type_field'   =>'date',
                            'class_field'  =>'form-control',
                            'id_field'     =>'',
                            'value_field'  =>(!empty($this->quiz_settings['quiz_start_at'])?$this->quiz_settings['quiz_start_at']:''),
                            'attributes'   =>'',
                          ]
                      );
              ?>
          </div>

        </div>
        <div class="clearfix"></div>
        <div class="form-group">
          <label class="label_choose_type">
              <span class="dashicons dashicons-admin-generic"></span>
              <?php _e('Quiz End At','QTS_Questions'); ?>
          </label>

          <div class="single-choices ">
              <?php  $this->add_fields(
                          [ 'name_field'   =>'quiz_end_at',
                            'type_field'   =>'date',
                            'class_field'  =>'form-control',
                            'id_field'     =>'',
                            'value_field'  =>(!empty($this->quiz_settings['quiz_end_at'])?$this->quiz_settings['quiz_end_at']:1),
                            'attributes'   =>'',
                          ]
                      );
              ?>
          </div>

        </div>
        <div class="clearfix"></div>
        
    </div>
</div>