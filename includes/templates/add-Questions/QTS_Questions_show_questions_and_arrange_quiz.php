<div class="conatiner">
    <div class="section-choose-type-Question">
    	<label class="label_choose_type">
         	<span class="dashicons dashicons-admin-generic"></span>
        	<?php _e('Arrange Questions','QTS_Questions'); ?>
        </label>
    	<?php   if(!empty($this->all_questions_for_quiz)): ?>
					<ul id="sortable">  
						<?php foreach ($this->all_questions_for_quiz as $question): $number_of_Question=1; ?>
					            <li class="item-<?php echo $number_of_Question; ?>">
					                <?php echo get_the_title($question->ID); ?> 	
								    <?php   
								        $this->add_fields(
					                          [ 'name_field'   =>'Question_ID_Arrange[]',
					                            'type_field'   =>'hidden',
					                            'class_field'  =>'form-control',
					                            'id_field'     =>'',
					                            'value_field'  =>(!empty($question->ID)?$question->ID:''),
					                            'attributes'   =>'',
					                          ]
				                        );
					                ?>	
					            </li>     
					    <?php $number_of_Question+=1; ?>	
					    <?php endforeach; ?>
					</ul> 
		<?php   endif; ?>
	</div>
</div>