jQuery(document).ready(function(){
    
    jQuery('.type_of_Question').click(function(){
        var type_Question = jQuery(this).val();
        var container_html='';
        if(type_Question=='sound'){
        	jQuery('.form-add-Question.active').slideUp('slow',function(){
               jQuery(this).removeClass('active');
               jQuery('.form-speech').slideDown('slow');
               jQuery('.form-speech').addClass('active');
        	});

        } 
        else if(type_Question=='text'){
        	jQuery('.form-add-Question.active').slideUp('slow',function(){
        	   jQuery(this).removeClass('active');
               jQuery('.form-text').slideDown('slow');
               jQuery('.form-text').addClass('active');
        	});
        } 
        else if(type_Question=='video'){
        	jQuery('.form-add-Question.active').slideUp('slow',function(){
        	   jQuery(this).removeClass('active');
               jQuery('.form-video').slideDown('slow');
               jQuery('.form-video').addClass('active');
        	});
        	
        } 
    });


    jQuery('.field_type_answer').click(function(){
        var type_answer = jQuery(this).val();
        var container_html='';
        if(type_answer=='sound'){
        	jQuery('.form-add-answer.active').slideUp('slow',function(){
               jQuery(this).removeClass('active');
               jQuery('.answer-speech').slideDown('slow');
               jQuery('.answer-speech').addClass('active');
        	});

        } 
        else if(type_answer=='choices'){
        	jQuery('.form-add-answer.active').slideUp('slow',function(){
        	   jQuery(this).removeClass('active');
               jQuery('.answer-choices').slideDown('slow');
               jQuery('.answer-choices').addClass('active');
        	});
        	
        } 
        else if(type_answer=='text'){
        	jQuery('.form-add-answer.active').slideUp('slow',function(){
        	   jQuery(this).removeClass('active');
               jQuery('.answer-text').slideDown('slow');
               jQuery('.answer-text').addClass('active');
        	});
        } 
    });

    var counter_question =Number(jQuery('.tr_table_choices:last-child>td>input').val())+1 || 1;

    jQuery('.add-new-choise').on('click',function(){
      var number_of_choice = counter_question+1;
    	jQuery('.choice'+counter_question).after('<tr class="tr_table_choices choice'+number_of_choice+'"><td> <label style="padding: 7px;">#choice'+number_of_choice+'</label> <input name="correct[]" type="checkbox" value="'+counter_question+'" class="form-control" /> </td><td> <input name="choices[]" value="" type="text" class="form-control" /> </td></tr>');
      counter_question+=1;
    });

});



 jQuery(function() {  
        jQuery( "#sortable" ).sortable({   
            placeholder: "ui-sortable-placeholder"   
        });  
    });  
