 var times_try =0;
jQuery('.form_solution_question').submit(function(e){
	times_try+=1;
    e.preventDefault();
    var answer_data = jQuery(this).serializeArray();
    var Question_ID = jQuery(this).attr('data-value');
    var time_number=jQuery('#container_playlist'+Question_ID).attr('data-times');
    if(times_try>=time_number){
    	jQuery('#next-question'+Question_ID).fadeIn('slow');
	    jQuery('#submit'+Question_ID).fadeOut('slow');
    }
    var data =  {   
                   Question_ID : Question_ID, 
                   answer_data : answer_data,
                   action: 'QST_Questions_check_choice_answer_is_true',
                };

	jQuery.ajax({
	      type:'POST',
	      url :QST_Questions_action_ajax.ajaxurl,
	      data:data,
	      success:function(result){ 
	         console.log(result);
	        if(result=='true'){
	        	jQuery('#next-question'+Question_ID).fadeIn('slow',function(){
	        		jQuery('.status_answer'+Question_ID).html('<i class="fas fa-check-circle"></i>');
	        		jQuery('.result_of_quiz').val(Number(jQuery('.result_of_quiz').val())+1);
	        	});
	        	jQuery('#submit'+Question_ID).fadeOut('slow');
	        	jQuery('input[type=checkbox]:checked').next().css({'color':'green'});

	        }
	        else if(result=='false'){
              jQuery('input[type=checkbox]:checked').next().css({'color':'red'});
              jQuery('.status_answer'+Question_ID).html('<i class="fas fa-times-circle"></i>');
	        }
	      }
	});
    
});



jQuery('.form_solution_correct_text').submit(function(e){
	times_try+=1;
    e.preventDefault();
    var answer_data = jQuery(this).serializeArray();
    var Question_ID = jQuery(this).attr('data-value');
    var time_number=jQuery('#container_playlist'+Question_ID).attr('data-times');
    if(times_try>=time_number){
    	jQuery('#next-question'+Question_ID).fadeIn('slow');
	    jQuery('#submit'+Question_ID).fadeOut('slow');
    }
    var data =  {   
                   Question_ID : Question_ID, 
                   answer_data : answer_data,
                   action: 'QST_Questions_check_text_answer_is_true',
                };

	jQuery.ajax({
	      type:'POST',
	      url :QST_Questions_action_ajax.ajaxurl,
	      data:data,
	      success:function(result){ 
	         console.log(result);
	        if(result=='true'){
	        	jQuery('#next-question'+Question_ID).fadeIn('slow',function(){
	        		jQuery('.status_answer'+Question_ID).html('<i class="fas fa-check-circle"></i>');
	        		jQuery('.result_of_quiz').val(Number(jQuery('.result_of_quiz').val())+1);
	        	});
	        	jQuery('#submit'+Question_ID).fadeOut('slow');
	        	jQuery('.QTS-container-input-text>*').css({'color':'green'});

	        }
	        else if(result=='false'){
              jQuery('.QTS-container-input-text>*').css({'color':'red'});
              jQuery('.status_answer'+Question_ID).html('<i class="fas fa-times-circle"></i>');
	        }
	      }
	});
    
});



 