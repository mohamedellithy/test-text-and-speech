 jQuery('.slider-for').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   easing:true,
   swipe:false,
   infinite:false,
   //touchMove:false,
   swipeToSlide:false,
   //accessibility:false,
 });


   var current =1;
   var transcript ='';
   var mobileRepeatBug = '';
   var noteContent = '';
   var old_value ='';
   var new_value ='';
   var Question_ID; 
   var times_try =0;

  jQuery('.next-question').on('click',function(){ 
     jQuery('.last-slide-result>.result').text(jQuery('.result_of_quiz').val()); 
     jQuery('.slider-for').on('afterChange', function(event, slick, currentSlide){
           /* write here some code*/
            var degree_of_test = jQuery('.result_of_quiz').val();
            var Quiz_ID        = jQuery('.next-question').attr('data-Quiz')
            var data =  {   
                   Quiz_ID : Quiz_ID, 
                   degree_of_test : degree_of_test,
                   action: 'QST_Quize_upgrade_degree_of_user',
                };
            jQuery.ajax({
                  type:'POST',
                  url :QST_Questions_action_ajax.ajaxurl,
                  data:data,
                  success:function(result){ 
                     console.log(result);
                    
                  }
            });

     });
        
     jQuery('.slider-for').slick("slickNext");
     jQuery(this).fadeOut('slow');

  });

  jQuery('.slider-for').on('afterChange', function(event, slick, currentSlide){
      if (slick.currentSlide >= slick.slideCount - slick.options.slidesToShow){
         var theDiv = jQuery('<div class="last-slide-result"><h3>Quiz Result</h3><div class="result"></div></div>');
         jQuery('.slider-for').slick('slickAdd', theDiv);
      }
  });


   //var instructions = jQuery('#recording-instructions').text();
   try {
     var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
     var recognition = new SpeechRecognition();
   }
   catch(e) {
     console.error(e);
     jQuery('.no-browser-support').show();
     jQuery('.app').hide();
   }
   recognition.onstart = function() { 
      jQuery('.recording_instructions'+Question_ID).fadeIn('slow');

   }

   recognition.onspeechend = function() {
      jQuery('.recording_instructions'+Question_ID).fadeOut('slow');
   }

   recognition.onerror = function(event) {
     if(event.error == 'no-speech') {
       jQuery('.recording_instructions'+Question_ID).fadeOut('slow');
        pauseRecording();
     };
   }

   recognition.onresult = function(event) {
     // event is a SpeechRecognitionEvent object.
     // It holds all the lines we have captured so far. 
     // We only need the current one.
     var current = event.resultIndex;
     /*var old_value = jQuery('.slick-active>.content-question>.container_playlist>tbody>tr>td>.icon_play').attr('data-value');
*/

     // Get a transcript of what was said.
     var transcript = event.results[current][0].transcript;
     console.log(event);

     // Add the current transcript to the contents of our Note.
     var noteContent = transcript;
     //jQuery('#note-textarea').val(noteContent);
     var mobileRepeatBug = (current == 1 && transcript == event.results[0][0].transcript);
     
      stopRecording();
      /*console.log(Question_ID);*/
      var mo = document.getElementById('container_playlist'+Question_ID);
      var di = document.createElement('span');
      var sect = document.createElement('div');

      di.append(noteContent);
      sect.append(di);
      mo.append(sect);
      di.classList.add('QTS_Question_text_speeck');

     /*******************************************************************/
        /*ajax*/
          var data = {   
                       Question_ID : Question_ID, 
                       action: 'QST_Questions_check_value_of_record',
                     };

          jQuery.ajax({
              type:'POST',
              url :QST_Questions_action_ajax.ajaxurl,
              data:data,
              success:function(result){
                  old_value = result;
                  if(old_value.toLowerCase()!=noteContent.toLowerCase()){
                      jQuery('.status_answer'+Question_ID).html('<i class="fas fa-times-circle"></i>');
                      di.classList.add('false_answer');
                  }
                  else if(old_value.toLowerCase()==noteContent.toLowerCase()){
                     di.classList.add('true_answer');
                     jQuery('#next-question'+Question_ID).fadeIn('slow',function(){
                        jQuery('.status_answer'+Question_ID).html('<i class="fas fa-check-circle"></i>');
                        jQuery('.result_of_quiz').val(Number(jQuery('.result_of_quiz').val())+1);
                     });
                     jQuery('.microphone'+Question_ID).fadeOut('slow');
                  }
              }
          });
 
     /******************************************************************/
      sect.classList.add('sect');
     
   }
   if(!mobileRepeatBug) {
     noteContent += transcript;
     //jQuery('#note-textarea').val(noteContent);
   }


/****************************************************************************************/
//webkitURL is deprecated but nevertheless
URL = window.URL || window.webkitURL;

var gumStream;            //stream from getUserMedia()
var rec;              //Recorder.js object
var input;              //MediaStreamAudioSourceNode we'll be recording

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext; //audio context to help us record

var recordButton = document.getElementsByClassName("spech_record");
/*var stopButton   = document.getElementById("stopButton");*/
/*var pauseButton  = document.getElementById("pauseButton");*/

//add events to those 2 buttons
for (var i = 0; i < recordButton.length; i++) {
  recordButton[i].addEventListener("click", startRecording);
}

/*stopButton.addEventListener("click", stopRecording);*/
/*pauseButton.addEventListener("click", pauseRecording);*/

function startRecording(e) {
  Question_ID = jQuery(this).attr('data-value');
  /* number of time for one question */
  
  console.log("recordButton clicked");

  /*
    Simple constraints object, for more advanced audio features see
    https://addpipe.com/blog/audio-constraints-getusermedia/
  */
    
    var constraints = { audio: true, video:false }

  /*
      Disable the record button until we get a success or fail from getUserMedia() 
  */

  recordButton.disabled = true;
  /*stopButton.disabled = false;*/
  /*pauseButton.disabled = false*/

  /*
      We're using the standard promise based getUserMedia() 
      https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
  */

  navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
    console.log("getUserMedia() success, stream created, initializing Recorder.js ...");

    /*
      create an audio context after getUserMedia is called
      sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
      the sampleRate defaults to the one set in your OS for your playback device

    */
    audioContext = new AudioContext();

    //update the format 
    //document.getElementById("formats").innerHTML="Format: 1 channel pcm @ "+audioContext.sampleRate/1000+"kHz"

    /*  assign to gumStream for later use  */
    gumStream = stream;
    
    /* use the stream */
    input = audioContext.createMediaStreamSource(stream);

    /* 
      Create the Recorder object and configure to record mono sound (1 channel)
      Recording 2 channels  will double the file size
    */
    rec = new Recorder(input,{numChannels:1})

    //start the recording process
    rec.record()

    console.log("Recording started");
    recognition.start();

  }).catch(function(err) {
      //enable the record button if getUserMedia() fails
      recordButton.disabled = false;
      /*stopButton.disabled = true;*/
      /*pauseButton.disabled = true*/
  });
}

function pauseRecording(){
  console.log("pauseButton clicked rec.recording=",rec.recording );
  if (rec.recording){
    //pause
    rec.stop();
   /* pauseButton.innerHTML="Resume";*/
  }else{
    //resume
    rec.record()
   /* pauseButton.innerHTML="Pause";*/

  }
}

function stopRecording() {
  console.log("stopButton clicked");

  //disable the stop button, enable the record too allow for new recordings
  /*stopButton.disabled = true;*/
  recordButton.disabled = false;
  /*pauseButton.disabled = true;*/

  //reset button just in case the recording is stopped while paused
  /*pauseButton.innerHTML="Pause";*/
  
  //tell the recorder to stop the recording
  rec.stop();

  

  //stop microphone access
  gumStream.getAudioTracks()[0].stop();

  //create the wav blob and pass it on to createDownloadLink
  rec.exportWAV(createDownloadLink);
}

function createDownloadLink(blob) {
  
  var url = URL.createObjectURL(blob);
  var au = document.createElement('audio');

  //name of .wav file to use during upload and download (without extendion)
  var filename = new Date().toISOString();
  //add controls to the <audio> element
  
  au.controls = true;
  au.src = url;
  au.classList.add('QTS_Question_audio');
  jQuery('#container_playlist'+Question_ID+'>.sect:last-of-type').prepend(au);
  
  var time_number = jQuery('#container_playlist'+Question_ID).attr('data-times'); 
  times_try+=1;
  if(times_try>=time_number){
    console.log('stop');
    jQuery('#next-question'+Question_ID).fadeIn('slow');
    jQuery('.microphone'+Question_ID).fadeOut('slow');
    
    return;
  }
  
  
}